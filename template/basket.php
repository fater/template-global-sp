<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="breadcrumbs">
            <a href="#">Главная</a>
            <span class="glyphicon glyphicon-chevron-right"></span>
            <a href="#">Корзина</a>
        </div>

        <h1>Корзина</h1>
        <div class="row">
            <div class="col-xs-4"></div>
            <div class="col-xs-8 text-right">
                <a href="#" class="gray">Вернуться к списку товаров</a>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-3"></div>
            <div class="col-xs-2"><b>Стоимость</b></div>
            <div class="col-xs-2"><b>Размер</b></div>
            <div class="col-xs-1"><b>Количество</b></div>
            <div class="col-xs-2 text-center"><b>Сумма</b></div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div class="right-icon">
                    <a href=""><span class="ion-close-circled site-icon"></span></a>
                </div>
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <div class="header-title-bold2">Костюм "Тюмень" песочный с черным</div>
                    <p>Артикул 40264</p>
                    <br>
                    <a href="#" class="orange"><span class="icon_flag"><span class="icon_notification active">+</span></span></a>
                    &nbsp;
                    <a href="#" class="orange">В закладки</a>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div class="right-icon">
                    <a href=""><span class="ion-close-circled site-icon"></span></a>
                </div>
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <div class="header-title-bold2">Костюм "Тюмень" песочный с черным</div>
                    <p>Артикул 40264</p>
                    <br>
                    <a href="#" class="orange"><span class="icon_flag"><span class="icon_notification active">+</span></span></a>
                    &nbsp;
                    <a href="#" class="orange">В закладки</a>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div class="right-icon">
                    <a href=""><span class="ion-close-circled site-icon"></span></a>
                </div>
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <div class="header-title-bold2">Костюм "Тюмень" песочный с черным</div>
                    <p>Артикул 40264</p>
                    <br>
                    <a href="#" class="orange"><span class="icon_flag"><span class="icon_notification active">+</span></span></a>
                    &nbsp;
                    <a href="#" class="orange">В закладки</a>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-9 basket_total">
                <hr>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-5"><b>Общий вес:</b> 55.6 кг</div>
                    <div class="col-xs-6"><b>Общая стоимость:</b> 126 740 Р</div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-5">
                        <b>Позиций:</b> 3
                        <p>
                            <b>Единиц товара:</b> 45
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <b>С оптовой скидкой:</b> <span class="orange">100 800 Р</span>
                        <div class="small-text">(Сумма заказа > 30 000 Р)</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-5"></div>
                    <div class="col-xs-3"><a href="#" class="orange">Пересчитать</a></div>
                    <div class="col-xs-3"><input type="button" class="button-orange" value="Оформить заказ"></div>
                </div>
            </div>
        </div>

        <hr>

        <div class="header-title-bold2">Удалены из корзины ранее</div>
        <div class="cart-block cart-block-noactive">
            <div class="block1">
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <div class="header-title-bold2">Костюм "Тюмень" песочный с черным</div>
                    <p>Артикул 40264</p>
                    <br>
                    <a href="#" class="orange">Вернуть в корзину</a>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
        </div>
    </div>

<?php
require_once '_footer.php';
?>