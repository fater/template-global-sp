<?php
require_once '_header.php';
?>

    <div class="container">
        <div id="slider1">
            <img src="../img/slider/1.png" class="active" />
            <img src="../img/slider/1.png" />
        </div>
    </div>

    <div class="container">
        <h1 class="text-center">Наши партнеры</h1>
        <div id="indexFormAnnounce">
            <div class="info-block-index">
                <div class="title">
                    <div class="icon"><img src="../img/icon_attention_small.png" alt=""></div>
                    <div class="spin text-right">
                        <span class="glyphicon glyphicon-remove small" id="indexFormAnnounceClose"></span></div>
                </div>
                <div class="content">
                    Розничная торговля осуществляется на территории магазина.
                    Перед приездом уточните наличие товара у менеджеров.
                    <br>
                    <br>
                    <b>
                        Оптовые цены действуют при сумме заказа от 30 000 Р
                    </b>
                </div>
            </div>
        </div>
        <div class="text-center">
            <img src="../img/partners.png" alt="" />
        </div>

        <br>
        <br>

        <h1 class="text-center">Каталог</h1>
        <div class="text-center media-middle">
            <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
            &nbsp;
            &nbsp;
            &nbsp;
            <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_price.png" alt="" />
        </div>
    </div>

    <div class="bg3">
        <div class="container">
            <div class="block">
                <div class="center_images">
                    <div><img src="../img/example.png" alt=""></div>
                    <div><img src="../img/example.png" alt=""></div>
                    <div><img src="../img/example.png" alt=""></div>
                    <div><img src="../img/example.png" alt=""></div>
                    <div><img src="../img/example.png" alt=""></div>
                    <div><img src="../img/example.png" alt=""></div>
                </div>
            </div>
            <div class="block">
                <div class="center_links">
                    <a href="#">Спецодежда</a>
                    <a href="#">Рабочая обувь</a>
                    <a href="#">Средства защиты</a>
                    <a href="#">Рабочие перчатки</a>
                    <a href="#">Головные уборы</a>
                    <a href="#">Бытовой текстиль</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h1 class="text-center"><img src="../img/icon_clothes.png" alt=""> Спецодежда</h1>
        <br>
        <div class="announce">
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
        </div>
        <div class="announce">
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
        </div>
    </div>

    <div class="container">
        <h1 class="text-center"><img src="../img/icon_footwear.png" alt=""> Рабочая обувь</h1>
        <br>
    </div>

    <div class="bg3">
        <div class="container">
            <div class="block">
                <div class="center_images">
                    <div>
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Обувь зимняя
                        </a>
                    </div>
                    <div>
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Обувь зимняя
                        </a>
                    </div>
                    <div>
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Обувь зимняя
                        </a>
                    </div>
                    <div>
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Обувь зимняя
                        </a>
                    </div>
                    <div>
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Обувь зимняя
                        </a>
                    </div>
                    <div>
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Обувь зимняя
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h1 class="text-center"><img src="../img/icon_protection.png" alt=""> Средства индивидуальной защиты</h1>
        <br>
        <div class="announce">
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Защиа головы, лица и глаз
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
        </div>
        <div class="announce">
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
            <div class="element">
                <a href="#" class="orange">
                    <img src="../img/example.png" alt=""><br />
                    Зимняя спецодежда
                </a>
            </div>
        </div>
    </div>

    <div id="slider">
        <div class="slide">
            <a href="#" class="product-announce">
                <div class="sticker1"><span>-20%</span></div>
                <img src="../img/example.png" alt=""><br />
                Костюм «Тюмень» песочный с черным
                <p>
                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                </p>
            </a>
        </div>
        <div class="slide">
            <a href="#" class="product-announce">
                <div class="sticker2"></div>
                <img src="../img/example.png" alt=""><br />
                Костюм «Тюмень»
                <p>
                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                </p>
            </a>
        </div>
        <div class="slide">
            <a href="#" class="product-announce">
                <div class="sticker3"></div>
                <img src="../img/example.png" alt=""><br />
                Костюм «Тюмень» песочный с черным
                <p>
                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                </p>
            </a>
        </div>
        <div class="slide">
            <a href="#" class="product-announce">
                <div class="sticker1"><span>-20%</span></div>
                <img src="../img/example.png" alt=""><br />
                Костюм «Тюмень»
                <p>
                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                </p>
            </a>
        </div>
        <div class="slide">
            <a href="#" class="product-announce">
                <div class="sticker1"><span>-20%</span></div>
                <img src="../img/example.png" alt=""><br />
                Костюм «Тюмень» песочный с черным
                <p>
                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                </p>
            </a>
        </div>
        <div class="slide">
            <a href="#" class="product-announce">
                <div class="sticker2"></div>
                <img src="../img/example.png" alt=""><br />
                Костюм «Тюмень» песочный с черным
                <p>
                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                </p>
            </a>
        </div>
        <div class="slide">
            <a href="#" class="product-announce">
                <div class="sticker3"></div>
                <img src="../img/example.png" alt=""><br />
                Костюм «Тюмень» песочный с черным
                <p>
                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                </p>
            </a>
        </div>
        <div class="slide">
            <a href="#" class="product-announce">
                <div class="sticker1"><span>-20%</span></div>
                <img src="../img/example.png" alt=""><br />
                Костюм «Тюмень» песочный с черным
                <p>
                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                </p>
            </a>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10 proposal_block">
                <h2>Наше предложение</h2>

                <p><a href="#">Индивидуальный подбор моделей для отдельных сотрудников и коллективов различных
                        компаний.</a>
                    Купить спецодежду оптом вы можете, воспользовавшись услугами наших консультантов. Специалисты
                    компании
                    «Глобал» подберут продукцию в соответствии со всеми условиями работы вашего предприятия.</p>

                <p><a href="#">Доставка товара по Москве и РФ.</a> При заказе от 100 000 рублей мы доставим вашу покупку
                    бесплатно в пределах столицы. Продажа в другие регионы предусматривает отправку груза транспортными
                    компаниями.</p>

                <p><a href="#">Скидки и дополнительные бонусы.</a> Покупая спецодежду оптом от производителя по
                    долгосрочному контракту, вы получаете особые льготные условия оплаты.</p>

                <p>Уточнить детали продажи продукции оптовыми партиями, а также сделать заказ на производство спецодежды
                    вы
                    можете по телефону +7 (495) 640 04 95.</p>
            </div>
            <div class="col-xs-1"></div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <p><img src="../img/icon_clothes.png" alt=""></p>
                <p><b>Богатый выбор
                        моделей</b></p>
                <p>Вы можете купить
                    рабочую спецодежду
                    в Москве различных
                    размеров на все сезоны. </p>
            </div>
            <div class="col-xs-3">
                <p><img src="../img/icon_tools.png" alt=""></p>
                <p><b>Возможность
                        комплектации заказа
                        обувью, головными
                        уборами и средствами
                        защиты</b></p>
                <p>Помимо производства
                    одежды, мы предлагаем
                    полную линейку всех
                    необходимых элементов
                    экипировки для работников
                    различных сфер.</p>
            </div>
            <div class="col-xs-3">
                <p><img src="../img/icon_purse.png" alt=""></p>
                <p><b>Лояльная
                        ценовая
                        политика</b></p>
                <p>Спецодежда в розницу
                    в интернет-магазине
                    производителя всегда
                    дешевле, чем
                    у посредников.
                    Работая с нами напрямую,
                    вы получаете продукцию
                    по минимальным ценам.</p>
            </div>
            <div class="col-xs-3">
                <p><img src="../img/icon_settings.png" alt=""></p>
                <p><b>Возможность
                        индивидуализации</b></p>
                <p>Наши специалисты имеют
                    богатый опыт в сфере
                    разработки уникальных
                    изделий любой сложности.</p>
            </div>
        </div>
    </div>

<?php
require_once '_footer.php';
?>