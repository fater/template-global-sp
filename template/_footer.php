

<div class="bg1">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 text-white small">
                <p>
                    Офис продаж<br />
                    г. Балашиха ул. Лукино вл. 49
                </p>

                <p>
                    Юридический адрес (Тендерный отдел)<br />
                    г. Москва, ул. Покровская, дом 1/13/6, строение 2, офис 35
                </p>

                <p>
                    e-mail: info@global-sp.ru
                </p>
            </div>
            <div class="col-xs-6 text-right">
                <div class="text1">+7 (495) 640 04 95</div>
                <div class="text2">с 8:00 до 20:00 без выходных</div>
            </div>
        </div>
    </div>
</div>

<div class="bg4">
    <div class="container">
        <br />
        <div class="row">
            <div class="col-xs-12 text-white small">
                <p><b>© 2009—2015 ООО ПКО "Глобал-Спецодежда"</b> – производство и продажа рабочей одежды оптом</p>
                <p>
                    Все права защищены. Вся приведенная на сайте информация носит справочный характер и не является
                    публичной офертой.<br />
                    Цвет моделей может незначительно отличаться от исходного. Товары, подлежащие сертификации,
                    сертифицированы.
                </p>
            </div>
        </div>
        <br>
    </div>
</div>

</body>
</html>