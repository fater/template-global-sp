<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-xs-8">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Каталог</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Летняя спецодежда</a>
                </div>
            </div>
            <div class="col-xs-4 text-right">
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
                &nbsp;
                &nbsp;
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_price.png" alt="" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <h2>Каталог</h2>

                <ul class="menu2">
                    <li><a href="#" class="ins">Спецодежда</a></li>
                    <li><a href="#">Зимняя спецодежда</a></li>
                    <li><a href="#">Летняя спецодежда</a></li>
                    <li><a href="#">Женская спецодежда</a></li>
                    <li><a href="#">Рабочие комбинезоны</a></li>
                    <li><a href="#">Рабочие костюмы</a></li>
                    <li><a href="#">Медицинская одежда</a></li>
                    <li><a href="#">Одежда для обслуживающего персонала</a></li>
                    <li><a href="#">Одежда для охранных и силовых структур</a></li>
                    <li><a href="#">Одежда со специальными защитными свойствами</a></li>
                    <li><a href="#">Сигнальная одежда</a></li>
                    <li><a href="#">Строительная одежда</a></li>
                    <li><a href="#">Камуфлированная одежда</a></li>

                    <li><a href="#" class="ins">Рабочая обувь</a></li>
                    <li><a href="#" class="ins">Средства индивидуальной защиты</a></li>
                    <li><a href="#" class="ins">Рабочие перчатки</a></li>
                    <li><a href="#" class="ins">Головные уборы</a></li>
                    <li><a href="#" class="ins">Бытовой текстиль</a></li>
                </ul>

                <?php
                require_once '_left_block.php';
                ?>

            </div>
            <div class="col-xs-9">

                <br>
                <div class="row">
                    <div class="col-xs-3">
                        Фильтр
                        <input type="checkbox">
                        &nbsp;
                        <input type="button" class="btn btn-orange" value="Параметры">
                    </div>
                    <div class="col-xs-2 text-right">
                        Сортировка:
                    </div>
                    <div class="col-xs-3">
                        <select name="" class="form-control">
                            <option value="">Уменьшение цены</option>
                            <option value="">Увеличение цены</option>
                            <option value="">Название</option>
                            <option value="">Новизна</option>
                            <option value="">Популярность</option>
                        </select>
                    </div>
                    <div class="col-xs-2 text-right">
                        Товаров на странице:
                    </div>
                    <div class="col-xs-2">
                        <select name="v3" class="form-control">
                            <option value="" selected>100</option>
                            <option value="">50</option>
                            <option value="">10</option>
                        </select>
                    </div>
                </div>

                <br>
                <div class="announce2">
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень» песочный с черным
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень»
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень» песочный с черным
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
                <br>
                <div class="announce2">
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень» песочный с черным
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень»
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень» песочный с черным
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php
require_once '_footer.php';
?>