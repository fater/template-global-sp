<br>
<div class="info-block">
    <div class="title" data-object="roll" data-name="element1">
        <div class="icon"><img src="../img/icon_parcel_small.png" alt=""></div>
        <div class="text">Доставка</div>
        <div class="spin"><span class="glyphicon glyphicon-chevron-up small"></span></div>
    </div>
    <div class="content" data-id="element1">
        <p>Заказы на сумму от 100 000 Р, осуществляется бесплатная
            доставка по Москве (в пределах
            МКАД).</p>
        <p>
            Также Вы сами может забрать
            заказ по адресу:
            г. Балашиха ул. Лукино вл. 49. </p>
    </div>

    <div class="title" data-object="roll" data-name="element2">
        <div class="icon"><img src="../img/icon_pocket_small.png" alt=""></div>
        <div class="text">Доставка</div>
        <div class="spin"><span class="glyphicon glyphicon-chevron-up small"></span></div>
    </div>
    <div class="content" data-id="element2">
        <p>Вы можете расплатиться за заказ наличными курьеру, который доставит по указанному адресу
            приобретенные изделия, а также документы, подтверждающие покупку;</p>
        <p>Также мы принимаем оплату заказов банковскими переводами на расчетный счет компании
            «Глобал-Спецодежда».</p>
    </div>

    <div class="title" data-object="roll" data-name="element3">
        <div class="icon"><img src="../img/icon_pocket_small.png" alt=""></div>
        <div class="text">Оптовикам</div>
        <div class="spin"><span class="glyphicon glyphicon-chevron-down small"></span></div>
    </div>
    <div class="content hidden" data-id="element3">
        <p>Вы можете расплатиться за заказ наличными курьеру, который доставит по указанному адресу
            приобретенные изделия, а также документы, подтверждающие покупку;</p>
        <p>Также мы принимаем оплату заказов банковскими переводами на расчетный счет компании
            «Глобал-Спецодежда».</p>
    </div>
</div>

<br>
<div class="info-block2">
    <div class="title">
        <div class="icon"><img src="../img/icon_attention_small.png" alt=""></div>
    </div>
    <div class="content">
        Узнать подробности или задать
        вопрос вы можете по телефону
        +7 (495) 640 04 95 или почте
        info@global-sp.ru.
    </div>
</div>