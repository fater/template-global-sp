<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Отзывы</a>
                </div>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
                &nbsp;
                &nbsp;
                <a href="#" class="orange">Скачать прайс</a> <img src="../img/icon_price.png" alt="" />
            </div>
        </div>

        <h1>Отзывы</h1>

        <div class="row">
            <div class="col-xs-12">

                <div class="shadow-block">
                    <div class="row">
                        <div class="col-xs-2">

                            <!-- SAMPLE IMAGE BEGIN -->
                            <div class="sample-image">Image</div>
                            <!-- SAMPLE IMAGE END -->

                        </div>
                        <div class="col-xs-10">
                            <div class="row" style="font-weight: 800 !important;">
                                <div class="col-xs-9">
                                    Алексей Петров
                                </div>
                                <div class="col-xs-3 text-right">
                                    4 мая 2016
                                </div>
                            </div>
                            <div class="standoff-block">
                                <div class="row">
                                    <div class="col-xs-12">
                                        «Качественная продукция надежной компании. Я уверен, что понадобились упорство о целеустремленность компании, чтобы создать первоклассную спецодежду. Вы молодцы.»
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="shadow-block">
                    <div class="row">
                        <div class="col-xs-2">

                            <!-- SAMPLE IMAGE BEGIN -->
                            <div class="sample-image">Image</div>
                            <!-- SAMPLE IMAGE END -->

                        </div>
                        <div class="col-xs-10">
                            <div class="row" style="font-weight: 800 !important;">
                                <div class="col-xs-9">
                                    Алексей Петров
                                </div>
                                <div class="col-xs-3 text-right">
                                    4 мая 2016
                                </div>
                            </div>
                            <div class="standoff-block">
                                <div class="row">
                                    <div class="col-xs-12">
                                        «Качественная продукция надежной компании. Я уверен, что понадобились упорство о целеустремленность компании, чтобы создать первоклассную спецодежду. Вы молодцы.»
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                <hr>
                <h3>Добавить отзыв</h3>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="standoff-block">
                            Имя
                            <input type="text" class="form-control" />
                        </div>

                        <div class="standoff-block">
                            Фамилия
                            <input type="text" class="form-control" />
                        </div>

                        <div class="standoff-block">
                            <a href="#" class="button-orange">Прикрепить фото</a>
                        </div>

                        <div class="standoff-block">
                            Код подтверждения
                            <div>
                                [Captcha code here]
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-8">
                        Текст отзыва
                        <textarea name="" id="" style="width: 100%;" rows="7" class="form-control"></textarea>
                        <div class=" standoff-block text-right">
                            <input type="submit" class="button-orange">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2"></div>
        </div>

        <br>
        <br>
        <br>

    </div>

<?php
require_once '_footer.php';
?>