<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="breadcrumbs">
            <a href="#">Главная</a>
            <span class="glyphicon glyphicon-chevron-right"></span>
            <a href="#">Закладки</a>
        </div>

        <h1>Закладки</h1>
        <div class="row">
            <div class="col-xs-4">
                <div class="list-group bookmarks-list-group">
                    <button type="button" class="list-group-item">Отобразить все</button>
                    <div class="splitter"></div>
                    <button type="button" class="list-group-item"><b>Купить через месяц</b></button>
                    <button type="button" class="list-group-item">Список 2</button>
                    <button type="button" class="list-group-item">Список обуви</button>
                </div>
                <div class="row">
                    <div class="col-xs-5">Новый список</div>
                    <div class="col-xs-5"><input type="text" class="form-control"></div>
                    <div class="col-xs-2"><input type="button" class="btn btn-orange" value="ок"></div>
                </div>
            </div>
            <div class="col-xs-8 text-right">
                <a href="#" class="gray">Вернуться к списку товаров</a>
            </div>
        </div>

        <br>
        <br>

        <div class="row">
            <div class="col-xs-6">
                <b>Купить через месяц</b>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">Удалить список целиком <span class="ion-close-circled site-icon"></span></a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-3"></div>
            <div class="col-xs-2"><b>Стоимость</b></div>
            <div class="col-xs-2"><b>Размер</b></div>
            <div class="col-xs-1"><b>Количество</b></div>
            <div class="col-xs-2 text-center"><b>Сумма</b></div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div class="right-icon">
                    <a href=""><span class="ion-close-circled site-icon"></span></a>
                </div>
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <div class="header-title-bold">Костюм "Тюмень" песочный с черным</div>
                    <p>Артикул 40264</p>
                    <a href="#" class="orange">На страницу товара</a>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-1">
                    Список
                </div>
                <div class="col-xs-3">
                    <label class="custom-select">
                        <select name="">
                            <option value="">купить через месяц</option>
                        </select>
                    </label>
                </div>
                <div class="col-xs-8 text-right">
                    <span class="icon_cart"><span class="icon_notification active">+</span></span>
                    &nbsp;
                    <input type="button" class="btn btn-orange" value="Добавить в корзину">
                </div>
            </div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div class="right-icon">
                    <a href=""><span class="ion-close-circled site-icon"></span></a>
                </div>
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <div class="header-title-bold">Костюм "Тюмень" песочный с черным</div>
                    <p>Артикул 40264</p>
                    <a href="#" class="orange">На страницу товара</a>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-1">
                    Список
                </div>
                <div class="col-xs-3">
                    <label class="custom-select">
                        <select name="">
                            <option value="">купить через месяц</option>
                        </select>
                    </label>
                </div>
                <div class="col-xs-8 text-right">
                    <span class="icon_cart"><span class="icon_notification active">+</span></span>
                    &nbsp;
                    <input type="button" class="btn btn-orange" value="Добавить в корзину">
                </div>
            </div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div class="right-icon">
                    <a href=""><span class="ion-close-circled site-icon"></span></a>
                </div>
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <div class="header-title-bold">Костюм "Тюмень" песочный с черным</div>
                    <p>Артикул 40264</p>
                    <a href="#" class="orange">На страницу товара</a>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-1">
                    Список
                </div>
                <div class="col-xs-3">
                    <label class="custom-select">
                        <select name="">
                            <option value="">купить через месяц</option>
                        </select>
                    </label>
                </div>
                <div class="col-xs-8 text-right">
                    <span class="icon_cart"><span class="icon_notification active">+</span></span>
                    &nbsp;
                    <input type="button" class="btn btn-orange" value="Добавить в корзину">
                </div>
            </div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div class="right-icon">
                    <a href=""><span class="ion-close-circled site-icon"></span></a>
                </div>
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <div class="header-title-bold">Костюм "Тюмень" песочный с черным</div>
                    <p>Артикул 40264</p>
                    <a href="#" class="orange">На страницу товара</a>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-1">
                    Список
                </div>
                <div class="col-xs-3">
                    <div>
                        <label class="custom-select">
                            <select name="">
                                <option value="">купить через месяц</option>
                            </select>
                        </label>
                    </div>

                </div>
                <div class="col-xs-8 text-right">
                    <span class="icon_cart"><span class="icon_notification active">+</span></span>
                    &nbsp;
                    <input type="button" class="btn btn-orange" value="Добавить в корзину">
                </div>
            </div>
        </div>

    </div>

<?php
require_once '_footer.php';
?>