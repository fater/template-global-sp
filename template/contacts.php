<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Контакты</a>
                </div>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
                &nbsp;
                &nbsp;
                <a href="#" class="orange">Скачать прайс</a> <img src="../img/icon_price.png" alt="" />
            </div>
        </div>

        <h1>Контакты</h1>

        <div class="row" style="font-size: 18px !important;">
            <div class="col-xs-6">

                <p>
                    <b>Офис продаж</b><br>
                    г. Балашиха ул. Лукино вл. 49
                </p>

                <p>
                    <b>Юридический адрес <br>
                        (тендерный отдел)</b><br>

                    101000 г. Москва, ул. Покровская, <br>
                    дом 1/13/6, строение 2, офис 35
                </p>

                <p>
                    <b>Email</b><br>
                    <a href="mailto:info@global-sp.ru" class="orange">info@global-sp.ru</a>
                </p>

                <p>
                    <b>Телефон/факс</b><br>

                    +7 (495) 640 04 95 <br>
                    +7 495 66 49 <br>
                    +7 730 66 39
                </p>

                <p>
                    <b>Время работы</b><br>

                    Пн.-Пт.: с 9.00 до 18.00. <br>
                    Сб., Вс. - выходные дни.
                </p>

                <p>
                    <b>Общественный <br>
                        транспорт</b><br>


                    Маршрутка 447 <br>
                    от ст. м. Щелковская

                </p>

            </div>
            <div class="col-xs-6">

                <b>Схема проезда:</b>
                <p>
                    [Схема]
                </p>

                <b>Яндекс карты:</b>
                <p>
                    [Яндекс карта]
                </p>

            </div>
        </div>
    </div>

<?php
require_once '_footer.php';
?>