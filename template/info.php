<?php
require_once '_header.php';
?>

    <!-- Modal -->
    <div class="modal fade" id="buyButton" tabindex="-1" role="dialog" aria-labelledby="buyButton">
        <div class="modal-dialog" role="document" style="width: 470px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Товар добавлен в корзину</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-5 text-right">
                            <img src="../img/example.png" alt="" style="height: 130px">
                        </div>
                        <div class="col-xs-7">
                            <b>Костюм "Тюмень" песочный с черным</b>
                            <p><b style="color: #BBB;">10 ед. х 4240 Р</b></p>
                            <span class="orange" style="font-size: 28px;"><b>42 400 р</b></span>
                        </div>
                    </div>
                </div>
                <div class="grid-modal-footer">
                    <div class="element">
                        <a href="#" class="button-orange-small text-nowrap">Перейти в корзину</a>
                    </div>
                    <div class="element">
                        <a href="#" class="button-orange-small text-nowrap">Продолжить покупки</a>
                    </div>
                    <div class="element text-nowrap">
                        <span class="icon_cart"><span class="icon_notification active">5</span></span>
                        <b>17 700 р</b>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-8">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Каталог</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Летняя спецодежда</a>
                </div>
            </div>
        </div>

        <h2>Костюм "Тюмень" песочный с черным</h2>

        <div class="row">
            <div class="col-xs-3">

                <ul class="menu2">
                    <li><a href="#" class="ins">Спецодежда</a></li>
                    <li><a href="#">Зимняя спецодежда</a></li>
                    <li><a href="#">Летняя спецодежда</a></li>
                    <li><a href="#">Женская спецодежда</a></li>
                    <li><a href="#">Рабочие комбинезоны</a></li>
                    <li><a href="#">Рабочие костюмы</a></li>
                    <li><a href="#">Медицинская одежда</a></li>
                    <li><a href="#">Одежда для обслуживающего персонала</a></li>
                    <li><a href="#">Одежда для охранных и силовых структур</a></li>
                    <li><a href="#">Одежда со специальными защитными свойствами</a></li>
                    <li><a href="#">Сигнальная одежда</a></li>
                    <li><a href="#">Строительная одежда</a></li>
                    <li><a href="#">Камуфлированная одежда</a></li>

                    <li><a href="#" class="ins">Рабочая обувь</a></li>
                    <li><a href="#" class="ins">Средства индивидуальной защиты</a></li>
                    <li><a href="#" class="ins">Рабочие перчатки</a></li>
                    <li><a href="#" class="ins">Головные уборы</a></li>
                    <li><a href="#" class="ins">Бытовой текстиль</a></li>
                </ul>

                <?php
                require_once '_left_block.php';
                ?>

            </div>
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-xs-5">
                        <img src="../img/example.png" style="width: 100%" alt="">
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-xs-3">
                                <img src="../img/example.png" class="img-responsive" alt="">
                            </div>
                            <div class="col-xs-3">
                                <img src="../img/example.png" class="img-responsive" alt="">
                            </div>
                            <div class="col-xs-3">
                                <img src="../img/example.png" class="img-responsive" alt="">
                            </div>
                            <div class="col-xs-3">
                                <img src="../img/example.png" class="img-responsive" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-7">
                        <div class="row">
                            <div class="col-xs-6">
                                Артикул <b>40264</b>
                            </div>
                            <div class="col-xs-6 text-right">
                                <a href="#" class="gray">Вернуться к списку товаров</a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-5">
                                <span style="line-height: normal;">
                                    <b>Опт</b><br>
                                    <span style="font-size: 36px;" class="orange"><b>42 400 р</b></span>
                                </span>
                            </div>
                            <div class="col-xs-7">
                                <span style="line-height: normal;">
                                    <b>Розница</b><br>
                                    <span style="font-size: 36px;" class="orange"><b>45 750 р</b></span>
                                </span>
                            </div>
                        </div>
                        <hr>
                        При заказе на сумму менее 30 000 р стоимость доставки по Москве 1500 р. Доступен самовывоз. В
                        случае самовывоза рекомендуем уточнить наличие товара у менеджера.
                        <hr>
                        <table class="table table-striped">
                            <tr>
                                <td>Материал</td>
                                <td>Оксфорд 210D+ Таслан 228Т</td>
                            </tr>
                            <tr>
                                <td>Плотность</td>
                                <td>25 г/м<sup>2</sup></td>
                            </tr>
                            <tr>
                                <td>Состав</td>
                                <td>100% п/э</td>
                            </tr>
                            <tr>
                                <td>Утеплитель (подкладка)</td>
                                <td>Тинсулейт 200 г/м<sup>2</sup></td>
                            </tr>
                            <tr>
                                <td>ГОСТ</td>
                                <td>ГОСТ Р 12.4.236-2011</td>
                            </tr>
                            <tr>
                                <td>Объем(М<sup>2</sup>)</td>
                                <td>0.02</td>
                            </tr>
                            <tr>
                                <td>Комплект</td>
                                <td>куртка</td>
                            </tr>
                            <tr>
                                <td>Вес товара</td>
                                <td>1.3 кг</td>
                            </tr>
                            <tr>
                                <td>Категория</td>
                                <td>Для особого и IV климатических поясов</td>
                            </tr>
                        </table>
                        <hr>
                        <div class="row">
                            <div class="col-xs-6">
                                <b>Размеры</b>
                            </div>
                            <div class="col-xs-6 text-right">
                                <a href="javascript:;" class="gray">
                                    Таблица размеров
                                    <div class="sizes">
                                        <table style="width: 100%;" class="table">
                                            <tr>
                                                <td></td>
                                                <td colspan="5">Мужские размеры</td>
                                                <td colspan="5">Женские размеры</td>
                                            </tr>
                                            <tr>
                                                <td>Обхват груди (см)</td>
                                                <td>44-46</td>
                                                <td>48-50</td>
                                                <td>52-54</td>
                                                <td>56-58</td>
                                                <td>60-62</td>
                                                <td>44-46</td>
                                                <td>48-50</td>
                                                <td>52-54</td>
                                                <td>56-58</td>
                                                <td>60-62</td>
                                            </tr>
                                            <tr>
                                                <td>Обхват талии (см)</td>
                                                <td>88-92</td>
                                                <td>96-100</td>
                                                <td>104-108</td>
                                                <td>112-116</td>
                                                <td>120-124</td>
                                                <td>88-92</td>
                                                <td>96-100</td>
                                                <td>104-108</td>
                                                <td>112-116</td>
                                                <td>120-124</td>
                                            </tr>
                                            <tr>
                                                <td>Обхват бедер (см)</td>
                                                <td>82-86</td>
                                                <td>90-94</td>
                                                <td>98-102</td>
                                                <td>106-110</td>
                                                <td>114-118</td>
                                                <td>96-100</td>
                                                <td>104-108</td>
                                                <td>112-116</td>
                                                <td>120-124</td>
                                                <td>126-132</td>
                                            </tr>
                                        </table>

                                        <table class="table">
                                            <tr>
                                                <td>Размер по обхвату груди</td>
                                                <td>46</td>
                                                <td>48</td>
                                                <td>50</td>
                                                <td>52</td>
                                                <td>54</td>
                                                <td>56</td>
                                                <td>58</td>
                                                <td>60</td>
                                                <td>62</td>
                                            </tr>
                                            <tr>
                                                <td>Размер по воротнику</td>
                                                <td>39</td>
                                                <td>40</td>
                                                <td>41</td>
                                                <td>42</td>
                                                <td>43</td>
                                                <td>44</td>
                                                <td>45</td>
                                                <td>46</td>
                                                <td>47</td>
                                            </tr>
                                        </table>

                                        <table class="table">
                                            <tr>
                                                <td rowspan="2">Соответствие размеров обуви</td>
                                                <td>23.5</td>
                                                <td>24.0</td>
                                                <td>24.5</td>
                                                <td>25.0</td>
                                                <td>25.5</td>
                                                <td>26.2</td>
                                                <td>27.0</td>
                                                <td>27.7</td>
                                                <td>28.5</td>
                                                <td>29.2</td>
                                                <td>30.0</td>
                                            </tr>
                                            <tr>
                                                <td>36</td>
                                                <td>37</td>
                                                <td>38</td>
                                                <td>39</td>
                                                <td>40</td>
                                                <td>41</td>
                                                <td>42</td>
                                                <td>43</td>
                                                <td>44</td>
                                                <td>45</td>
                                                <td>46</td>
                                            </tr>
                                        </table>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <br>
                        <div data-object="amount_box">
                            <div class="row">
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    88-92/170-176
                                    <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_minus">-</button>
                                </span>
                                        <input type="text" class="form-control2 text-center" value="0">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-object="amount_plus">+</button>
                                </span>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                <span class="icon_cart"><span class="icon_notification active">+</span></span>
                                &nbsp;
                                <div class="position">
                                    <div class="helper hidden" data-object="addCardNotifier">
                                        Ошибка: выберите размер и количество
                                    </div>
                                </div>
                                <input type="button" class="btn btn-orange" value="Добавить в корзину" data-toggle="modal" data-target="#buyButton" data-object="buyButton">
                            </div>
                            <div class="col-xs-6 text-right">
                                <span class="icon_flag"><span class="icon_notification_active">+</span></span>
                                &nbsp;
                                &nbsp;
                                <a href="#" class="orange">
                                    В закладки
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <h2 class="text-center">Похожие товары</h2>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="#" class="product-announce-active">
                            <span class="button-orange">Купить</span>
                            <img src="../img/example.png" alt=""><br />
                            Костюм «Тюмень» песочный с черным
                            <p>
                                <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                            </p>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="product-announce-active">
                            <span class="button-orange">Купить</span>
                            <img src="../img/example.png" alt=""><br />
                            Костюм «Тюмень» песочный с черным
                            <p>
                                <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                            </p>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="product-announce-active">
                            <span class="button-orange">Купить</span>
                            <img src="../img/example.png" alt=""><br />
                            Костюм «Тюмень» песочный с черным
                            <p>
                                <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                            </p>
                        </a>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="#" class="product-announce-active">
                            <span class="button-orange">Купить</span>
                            <img src="../img/example.png" alt=""><br />
                            Костюм «Тюмень» песочный с черным
                            <p>
                                <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                            </p>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="product-announce-active">
                            <span class="button-orange">Купить</span>
                            <img src="../img/example.png" alt=""><br />
                            Костюм «Тюмень» песочный с черным
                            <p>
                                <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                            </p>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="product-announce-active">
                            <span class="button-orange">Купить</span>
                            <img src="../img/example.png" alt=""><br />
                            Костюм «Тюмень» песочный с черным
                            <p>
                                <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                            </p>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php
require_once '_footer.php';
?>