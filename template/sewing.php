<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Пошив</a>
                </div>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
                &nbsp;
                &nbsp;
                <a href="#" class="orange">Скачать прайс</a> <img src="../img/icon_price.png" alt="" />
            </div>
        </div>

        <h1>Пошив спецодежды на заказ в Москве</h1>

        <div class="row">
            <div class="col-xs-12">

                Компания «Глобал» предлагает пошив спецодежды на заказ в Москве и по всей России на основе предоставленного клиентом описания, спецификации, технической документации, образца, фотографии или эскиза. Также возможно внесение изменений в существующие модели согласно пожеланиям заказчика и производственной необходимости. Изделия, изготовленные индивидуально, помогают формированию корпоративного стиля и повышают узнаваемость компании. Кроме того, они способствуют увеличению работоспособности сотрудников более чем на 17 %

                <br>
                <h3>Этапы изготовления</h3>
                <br>

                <div class="standoff-block">
                    <div class="row">
                        <div class="col-xs-2 text-right">
                            <img src="../img/icon_clothes_gray.png" alt="">
                        </div>
                        <div class="col-xs-5">
                            Разработка эскизов или получение их от клиента.
                        </div>
                    </div>
                </div>
                <div class="standoff-block">
                    <div class="row">
                        <div class="col-xs-2 text-right">
                            <img src="../img/icon_parcel_small.png" alt="">
                        </div>
                        <div class="col-xs-5">
                            Изготовление опытных образцов
                            и их предоставление на пробное использование.
                        </div>
                    </div>
                </div>
                <div class="standoff-block">
                    <div class="row">
                        <div class="col-xs-2 text-right">
                            <img src="../img/icon_men_cubes_small.png" alt="">
                        </div>
                        <div class="col-xs-7">
                            Изготовление партии одежды <br>
                            в фирменном стиле.
                        </div>
                    </div>
                </div>

                <br>

                <h3>Преимущества пошива спецодежды в компании «Глобал»</h3>

                <div class="standoff-block">
                    Индивидуальный подход. Использование современных материалов и технологий позволяет нам изготавливать спецодежду для любых сфер деятельности, а оптимальное сочетание цены и качества – сотрудничать с клиентами разного уровня. Заказы отшиваются малыми (от 200 единиц) и большими партиями, а перед их выпуском клиенту обязательно предоставляется образец.
                </div>
                <div class="standoff-block">
                    Высокое качество изделий. В процессе изготовления моделей на заказ применяются огне-, термо- и морозостойкие, антистатические, мембранные, ветрозащитные, водонепроницаемые и другие виды тканей. Эргономичные силуэты обеспечивают оптимальный комфорт ношения, а прочность и износоустойчивость используемых материалов гарантируют долговечность и презентабельный вид изделий в течение всего срока службы.
                </div>
                <div class="standoff-block">
                    Широкий размерный ряд. Возможен пошив моделей стандартного размера (от 44 до 62) на рост 182–188 или 170–176 для мужчин и 158–164 и 170–176 для женщин, а также изготовление изделий на заказ по нестандартным меркам. Стоимость в этом случае рассчитывается в индивидуальном порядке.
                </div>
                <div class="standoff-block">
                    Получить более подробную информацию и заказать пошив спецодежды в интернет-магазине в Москве можно по телефону +7 (495) 640-04-95 или по электронной почте info@global-sp.ru.
                </div>
                <div class="standoff-block text-center">
                    <a href="#" class="button-orange">Заказать звонок</a>
                </div>

                <br>
                <p><b>Примеры нашей продукции</b></p>
                <div class="announce2">
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень» песочный с черным
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень» песочный с черным
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень»
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="product-announce-active">
                            <img src="../img/example.png" alt=""><br />
                            <div class="bg">
                                <span class="button-orange">Купить</span>
                            </div>
                            <div class="text">
                                Костюм «Тюмень» песочный с черным
                                <p>
                                    <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                                    <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

<?php
require_once '_footer.php';
?>