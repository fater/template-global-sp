<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Оптовикам</a>
                </div>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
                &nbsp;
                &nbsp;
                <a href="#" class="orange">Скачать прайс</a> <img src="../img/icon_price.png" alt="" />
            </div>
        </div>

        <h1>Оптовикам</h1>

        <div class="row">
            <div class="col-xs-12">

                Компания «Глобал-Спецодежда» предлагает удобные условия для оптовых покупателей. Являясь производителем
                высококачественной спецодежды, мы готовы предоставить Вам наиболее полный ассортимент защитных костюмов,
                средств индивидуальной защиты и надежной обуви, независимо от сферы деятельности Вашего предприятия.
                <br>
                <br>
                «Глобал-Спецодежда» предлагает:
                <br>
                <br>

                <div class="standoff-block">
                    <div class="row">
                        <div class="col-xs-2 text-right">
                            <img src="../img/icon_pocket_small.png" alt="">
                        </div>
                        <div class="col-xs-7">
                            <p><b>доступные цены.</b> Работая с нами, Вы получите возможность приобретать
                                качественную спецодежду и обувь по ценам производителя.</p>

                            <p>Отсутствие каких-либо посредников делает стоимость наших
                                товаров привлекательно низкой;</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2 text-right">
                            <img src="../img/icon_parcel_small.png" alt="">
                        </div>
                        <div class="col-xs-7">
                            <p><b>бесплатную доставку</b> по Москве. Нашим оптовым покупателям
                                доступна бесплатная доставка товаров.</p>

                            <p>При заказе на сумму свыше 100 000 рублей наша служба привезет
                                приобретенную спецодежду в пределах Москвы без дополнительной оплаты услуги;</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2 text-right">
                            <img src="../img/icon_cubes_small.png" alt="">
                        </div>
                        <div class="col-xs-7">
                            <b>хорошие скидки.</b> Покупая у нас защитные костюмы, обувь
                            и другие изделия оптовыми партиями, Вы будете получать
                            от нашей компании скидки и бонусы, которые сделают закупку
                            еще более выгодной для Вашего предприятия.
                        </div>
                    </div>
                </div>

                <br>

                <div class="standoff-block">
                    Более подробно об условиях сотрудничества компании «Глобал-Спецодежда» с оптовыми покупателями можно узнать по телефону +7 (495) 640 04 95 или с помощью отправки сообщения на
                    <a href="mailto:info@global-sp.ru">info@global-sp.ru</a>.
                </div>

                <div class="standoff-block text-center">
                    <a href="#" class="button-orange">Заказать звонок</a>
                </div>

                <br>

                <b>Каталог продукции 2014-2015</b>

                <div class="standoff-block">
                    <div class="row">
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                    </div>
                </div>

                <div class="standoff-block">
                    <div class="row">
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                    </div>
                </div>

                <div class="standoff-block">
                    <div class="row">
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                        <div class="col-xs-2">
                            <div class="sample-image">Image</div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>

<?php
require_once '_footer.php';
?>