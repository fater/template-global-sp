<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Новости</a>
                </div>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
                &nbsp;
                &nbsp;
                <a href="#" class="orange">Скачать прайс</a> <img src="../img/icon_price.png" alt="" />
            </div>
        </div>

        <h1>Новости</h1>

        <div class="row">
            <div class="col-xs-6">
                <div class="grid">
                    <div class="element">
                        <b>Архив за</b>
                    </div>
                    <div class="element">
                        <label class="custom-select">
                            <select name="">
                                <option value="">2016</option>
                                <option value="">2015</option>
                                <option value="">2014</option>
                                <option value="">2013</option>
                                <option value="">2012</option>
                            </select>
                        </label>
                    </div>
                    <div class="element text-right">
                        <b>Новостей на странице</b>
                    </div>
                    <div class="element">
                        <label class="custom-select">
                            <select name="">
                                <option value="">100</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">1</a>
                <a href="#" class="orange active">2</a>
                <a href="javascript:;" class="orange">..</a>
                <a href="#" class="orange">10</a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                <div class="shadow-block">
                    <div class="row">
                        <div class="col-xs-2">

                            <!-- SAMPLE IMAGE BEGIN -->
                            <div class="sample-image">Image</div>
                            <!-- SAMPLE IMAGE END -->

                        </div>
                        <div class="col-xs-10">
                            <div class="row" style="font-weight: 800 !important;">
                                <div class="col-xs-9">
                                    Сайт Global-sp.ru на реконструкции
                                </div>
                                <div class="col-xs-3 text-right">
                                    4 мая 2016
                                </div>
                            </div>
                            <div class="standoff-block">
                                <div class="row">
                                    <div class="col-xs-12">
                                        Уважаемые клиенты, сайт Global-sp.ru находится на реконструкции, цены указанные
                                        в
                                        каталоге недействительны! Просим прощения за неудобства, вскоре мы вернемся к
                                        Вам с
                                        новыми уникальными предложениями на рынке спец.одежды!
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-xs-offset-8 text-right">
                                    <a href="#" class="orange">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="shadow-block">
                    <div class="row">
                        <div class="col-xs-2">

                            <!-- SAMPLE IMAGE BEGIN -->
                            <div class="sample-image">Image</div>
                            <!-- SAMPLE IMAGE END -->

                        </div>
                        <div class="col-xs-10">
                            <div class="row" style="font-weight: 800 !important;">
                                <div class="col-xs-9">
                                    Сайт Global-sp.ru на реконструкции
                                </div>
                                <div class="col-xs-3 text-right">
                                    4 мая 2016
                                </div>
                            </div>
                            <div class="standoff-block">
                                <div class="row">
                                    <div class="col-xs-12">
                                        Уважаемые клиенты, сайт Global-sp.ru находится на реконструкции, цены указанные
                                        в
                                        каталоге недействительны! Просим прощения за неудобства, вскоре мы вернемся к
                                        Вам с
                                        новыми уникальными предложениями на рынке спец.одежды!
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-xs-offset-8 text-right">
                                    <a href="#" class="orange">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="shadow-block">
                    <div class="row">
                        <div class="col-xs-2">

                            <!-- SAMPLE IMAGE BEGIN -->
                            <div class="sample-image">Image</div>
                            <!-- SAMPLE IMAGE END -->

                        </div>
                        <div class="col-xs-10">
                            <div class="row" style="font-weight: 800 !important;">
                                <div class="col-xs-9">
                                    Сайт Global-sp.ru на реконструкции
                                </div>
                                <div class="col-xs-3 text-right">
                                    4 мая 2016
                                </div>
                            </div>
                            <div class="standoff-block">
                                <div class="row">
                                    <div class="col-xs-12">
                                        Уважаемые клиенты, сайт Global-sp.ru находится на реконструкции, цены указанные
                                        в
                                        каталоге недействительны! Просим прощения за неудобства, вскоре мы вернемся к
                                        Вам с
                                        новыми уникальными предложениями на рынке спец.одежды!
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-xs-offset-8 text-right">
                                    <a href="#" class="orange">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="shadow-block">
                    <div class="row">
                        <div class="col-xs-2">

                            <!-- SAMPLE IMAGE BEGIN -->
                            <div class="sample-image">Image</div>
                            <!-- SAMPLE IMAGE END -->

                        </div>
                        <div class="col-xs-10">
                            <div class="row" style="font-weight: 800 !important;">
                                <div class="col-xs-9">
                                    Сайт Global-sp.ru на реконструкции
                                </div>
                                <div class="col-xs-3 text-right">
                                    4 мая 2016
                                </div>
                            </div>
                            <div class="standoff-block">
                                <div class="row">
                                    <div class="col-xs-12">
                                        Уважаемые клиенты, сайт Global-sp.ru находится на реконструкции, цены указанные
                                        в
                                        каталоге недействительны! Просим прощения за неудобства, вскоре мы вернемся к
                                        Вам с
                                        новыми уникальными предложениями на рынке спец.одежды!
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-xs-offset-8 text-right">
                                    <a href="#" class="orange">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 text-right">
                <a href="#" class="orange">1</a>
                <a href="#" class="orange active">2</a>
                <a href="javascript:;" class="orange">..</a>
                <a href="#" class="orange">10</a>
            </div>
        </div>
    </div>

<?php
require_once '_footer.php';
?>