<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ГЛОБАЛ СП</title>
    <meta name="viewport" content="width=device-width, initial-scale=0.25">
    <meta name="MobileOptimized" content="1170"/>
    <meta name="HandheldFriendly" content="true"/>
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap-theme.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/app.css" />
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="../bower_components/bxslider-4/dist/jquery.bxslider.min.js"></script>
    <link rel="stylesheet" href="../bower_components/bxslider-4/dist/jquery.bxslider.min.css" />

    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
</head>
<body>

<div class="bg1">
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <a href="/" class="logo"></a>
            </div>
            <div class="col-xs-5"></div>
            <div class="col-xs-4 text-right">
                <div class="text1">+7 (495) 640 04 95</div>
                <div class="text2">с 8:00 до 20:00 без выходных</div>
                <div class="text3">
                    <a href="javascript:;" data-toggle="modal" data-target="#loginForm">Войти</a> или
                    <a href="javascript:;" data-toggle="modal" data-target="#registrationForm">зарегистрироваться</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Login -->
<div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-labelledby="loginForm">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-bg-dark">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Войти на сайт</h3>
            </div>
            <div class="modal-body">
                <div class="body-half">
                    Логин:
                    <p><input type="text" class="form-control"></p>
                    Пароль:
                    <p><input type="text" class="form-control"></p>
                    <p><input type="checkbox" > Запомнить меня</p>
                    <p class="text-center"><input type="submit" class="button-orange" value="Войти"></p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    Аккаунта еще нет? <a href="javascript:;" class="orange" data-toggle="modal" data-target="#registrationForm">Регистрация</a><br>
                    Забыли пароль? <a href="javascript:;" class="orange" data-toggle="modal" data-target="#forgotForm">Восстановить</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Forgot -->
<div class="modal fade" id="forgotForm" tabindex="-1" role="dialog" aria-labelledby="forgotForm">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-bg-dark">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Забыли пароль?</h3>
            </div>
            <div class="modal-body">
                <div class="body-half">
                    Ваша почта:
                    <p><input type="text" class="form-control"></p>
                    <p class="text-center"><input type="submit" class="button-orange" value="Восстановить пароль"></p>
                </div>
            </div>
            <div class="modal-footer-white">
                <div class="text-center">
                    Вы можете сбросить пароль через эту форму. Если вы утеряли доступ к вашей почте, свяжитесь с
                    <a href="#" class="orange">поддержкой</a>
                </div>
            </div>
            <div class="orange-block">
                Этот почтовый адрес отсутствует в нашей базе
            </div>
        </div>
    </div>
</div>

<!-- Modal Registration -->
<div class="modal fade" id="registrationForm" tabindex="-1" role="dialog" aria-labelledby="registrationForm">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-bg-dark">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Регистрация</h3>
            </div>
            <div class="modal-body">
                <div class="body-half">
                    Ваша почта:
                    <p><input type="text" class="form-control"></p>
                    Пароль:
                    <p><input type="password" class="form-control"></p>
                    Пароль повторно:
                    <p><input type="password" class="form-control"></p>
                    <p class="text-center"><input type="submit" class="button-orange" value="Зарегистрировать"></p>
                </div>
            </div>
            <div class="orange-block">
                Введенные пароли не совпадают
            </div>
        </div>
    </div>
</div>

<!-- Modal Call -->
<div class="modal fade" id="callForm" tabindex="-1" role="dialog" aria-labelledby="callForm">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-bg-dark">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Обратный звонок</h4>
            </div>
            <div class="modal-body">
                Имя: *
                <input type="text" class="form-control">
                <br>
                Телефон: *
                <input type="text" class="form-control">
                <br>
                Комментарий:
                <textarea name="" id="" style="width: 100%" rows="6" class="form-control"></textarea>
                <br>
                Время звонка: **
                <br>
                с <select name="" id="" class="form-control">
                    <option value="4">9:00</option>
                    <option value="5">10:00</option>
                    <option value="6">11:00</option>
                    <option value="7">12:00</option>
                    <option value="8">13:00</option>
                    <option value="9">14:00</option>
                    <option value="10">15:00</option>
                    <option value="11">16:00</option>
                    <option value="12">17:00</option>
                    <option value="13">18:00</option>
                    <option value="14">19:00</option>
                    <option value="15">20:00</option>
                </select>

                до <select name="" id="" class="form-control">
                    <option value="16">9:00</option>
                    <option value="17">10:00</option>
                    <option value="18">11:00</option>
                    <option value="19">12:00</option>
                    <option value="20">13:00</option>
                    <option value="21">14:00</option>
                    <option value="22">15:00</option>
                    <option value="23">16:00</option>
                    <option value="24">17:00</option>
                    <option value="25">18:00</option>
                    <option value="26">19:00</option>
                    <option value="27">20:00</option>
                </select>

                <br>
                <input type="submit" value="Перезвонить мне" id="" class="button-orange">
                <p>
                    * - Поля, обязательные для заполнения
                </p>
            </div>
        </div>
    </div>
</div>

<div class="bg2">
    <div class="container">
        <div class="blocks">
            <div class="block"><a href="." class="home_button"></a></div>
            <div class="block">
                <ul class="menu">
                    <li><a href="catalog.php">Каталог</a></li>
                    <li><a href="" class="active">О компании</a>
                        <ul>
                            <li><a href="#">Отзывы</a></li>
                            <li><a href="#" class="arrow">Вопрос-ответ</a>
                                <ul>
                                    <li><a href="#">Отзывы</a></li>
                                    <li><a href="#">Вопрос-ответ</a></li>
                                    <li><a href="#">Общие сведения</a></li>
                                    <li><a href="#">Сертификаты</a></li>
                                    <li><a href="#">Дипломы и награды</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Общие сведения</a></li>
                            <li><a href="#">Сертификаты</a></li>
                            <li><a href="#">Дипломы и награды</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Услуги</a></li>
                    <li><a href="#">Вакансии</a></li>
                    <li><a href="news.php">Новости</a></li>
                    <li><a href="#">Статьи</a></li>
                    <li><a href="contacts.php">Контакты</a></li>
                </ul>
            </div>
            <div class="splitter"></div>
            <div class="searchform hidden" id="searchFormBlock">
                <div class="block">
                    <div class="icon" id="searchFormClose"></div>
                    <div class="field">
                        <input type="text" class="form-control" name="">
                    </div>
                    <div class="button">
                        <input type="submit" value="Искать" class="button-orange">
                    </div>
                </div>
            </div>
            <div class="block"><span class="icon_lens" id="searchFormOpen"></span></div>
            <div class="splitter"></div>
            <div class="block text-nowrap">
                <a href="#" class="cart_button">
                    <span class="icon_cart"><span class="icon_notification active">55</span></span>
                    17 700 р
                </a>
            </div>
            <div class="splitter"></div>
            <div class="block">
                <span class="icon_flag"><span class="icon_notification">0</span></span>
            </div>
            <div class="splitter"></div>
            <div class="block text-center">
                <a href="javascript:;" class="button-orange" data-toggle="modal" data-target="#callForm">Заказать звонок</a>
            </div>
        </div>
    </div>
</div>
