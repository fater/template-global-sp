<?php
require_once '_header.php';
?>

    <!-- Modal Filter -->
    <div class="modal fade" id="filterForm" tabindex="-1" role="dialog" aria-labelledby="filterForm">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title modal-title" id="myModalLabel">Параметры фильтра</h4>
                </div>
                <form action="" class="form-inline">
                    <div class="modal-body">
                        <div class="title-bold">Цена</div>
                        <div class="form-group">
                            <input type="text" name="" placeholder="0 000 000" class="form-control" style="width: 90px;">
                        </div>
                        <div class="form-group">
                            —
                        </div>
                        <div class="form-group">
                            <input type="text" name="" placeholder="9 999 999" class="form-control" style="width: 90px;">
                        </div>
                        <div class="form-group title-bold">
                            Р
                            &nbsp;
                            &nbsp;
                            &nbsp;
                        </div>
                        <div class="form-group custom-checkbox">
                            <input id="btn_cat_1" type="checkbox" checked="checked">
                            <label for="btn_cat_1">Любая</label>
                        </div>

                        <div class="title-bold"><br>Цвета</div>
                        <div class="grid">
                            <div class="element custom-checkbox">
                                <input id="btn_c2_1" type="checkbox" checked="checked">
                                <label for="btn_c2_1">
                                    <img src="../img/materials/1.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Бежевый</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_2" type="checkbox" checked="checked">
                                <label for="btn_c2_2">
                                    <img src="../img/materials/2.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Белый</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_3" type="checkbox" checked="checked">
                                <label for="btn_c2_3">
                                    <img src="../img/materials/3.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Бордо</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_4" type="checkbox" checked="checked">
                                <label for="btn_c2_4">
                                    <img src="../img/materials/4.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Василек</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_5" type="checkbox" checked="checked">
                                <label for="btn_c2_5">
                                    <img src="../img/materials/5.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Зеленый</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_6" type="checkbox" checked="checked">
                                <label for="btn_c2_6">
                                    <img src="../img/materials/6.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Камуфляж</div>
                                </label>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="element custom-checkbox">
                                <input id="btn_c2_7" type="checkbox" checked="checked">
                                <label for="btn_c2_7">
                                    <img src="../img/materials/7.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Коричневый</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_8" type="checkbox" checked="checked">
                                <label for="btn_c2_8">
                                    <img src="../img/materials/8.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Красный</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_9" type="checkbox" checked="checked">
                                <label for="btn_c2_9">
                                    <img src="../img/materials/9.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Серый</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_10" type="checkbox" checked="checked">
                                <label for="btn_c2_10">
                                    <img src="../img/materials/10.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Сигнальный</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_11" type="checkbox" checked="checked">
                                <label for="btn_c2_11">
                                    <img src="../img/materials/11.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Синий</div>
                                </label>
                            </div>
                            <div class="element custom-checkbox">
                                <input id="btn_c2_12" type="checkbox" checked="checked">
                                <label for="btn_c2_12">
                                    <img src="../img/materials/12.png" class="catalog-label-image" alt=""><br>
                                    <div class="catalog-label-gray">Черный</div>
                                </label>
                            </div>
                        </div>

                        <div class="title-bold"><br>ГОСТ</div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c3_1" type="checkbox" checked="checked">
                            <label for="btn_c3_1">12.4.219-99</label>
                            &nbsp;
                            &nbsp;
                        </div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c3_2" type="checkbox">
                            <label for="btn_c3_2">25295-2003</label>
                            &nbsp;
                            &nbsp;
                        </div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c3_3" type="checkbox">
                            <label for="btn_c3_3">P 12.4.236-2011</label>
                        </div>

                        <div class="title-bold"><br>Комплект</div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c4_1" type="checkbox" checked="checked">
                            <label for="btn_c4_1">Брюки</label>
                            &nbsp;
                            &nbsp;
                        </div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c4_2" type="checkbox">
                            <label for="btn_c4_2">Жилет</label>
                            &nbsp;
                            &nbsp;
                        </div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c4_3" type="checkbox">
                            <label for="btn_c4_3">Куртка</label>
                            &nbsp;
                            &nbsp;
                        </div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c4_4" type="checkbox">
                            <label for="btn_c4_4">Полукомбинезон</label>
                        </div>

                        <div class="title-bold"><br>Защитные свойства</div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c5_1" type="checkbox" checked="checked">
                            <label for="btn_c5_1">2-й класс сигнальной защиты</label>
                            &nbsp;
                            &nbsp;
                        </div>
                        <div class="form-group custom-checkbox-text">
                            <input id="btn_c5_2" type="checkbox">
                            <label for="btn_c5_2">От механического истирания</label>
                            &nbsp;
                            &nbsp;
                        </div>
                    </div>
                    <div class="modal-footer text-right">
                        <input type="submit" value="Применить" class="button-orange">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <input type="reset" value="Сбросить" class="button-orange">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-8">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Каталог</a>
                </div>
            </div>
            <div class="col-xs-4 text-right">
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
                &nbsp;
                &nbsp;
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_price.png" alt="" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <h2>Каталог</h2>

                <ul class="menu2">
                    <li><a href="" class="arrow active">Спецодежда</a>
                        <ul>
                            <li><a href="#">Зимняя спецодежда</a></li>
                            <li><a href="" class="arrow">Спецодежда</a>
                                <ul>
                                    <li><a href="#">Зимняя спецодежда</a></li>
                                    <li><a href="#">Летняя спецодежда</a></li>
                                    <li><a href="#">Женская спецодежда</a></li>
                                    <li><a href="#">Рабочие комбинезоны</a></li>
                                    <li><a href="#">Рабочие костюмы</a></li>
                                    <li><a href="#">Медицинская одежда</a></li>
                                    <li><a href="#">Одежда для обслуживающего персонала</a></li>
                                    <li><a href="#">Одежда для охранных и силовых структур</a></li>
                                    <li><a href="#">Одежда со специальными защитными свойствами</a></li>
                                    <li><a href="#">Сигнальная одежда</a></li>
                                    <li><a href="#">Строительная одежда</a></li>
                                    <li><a href="#">Камуфлированная одежда</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Летняя спецодежда</a></li>
                            <li><a href="#">Женская спецодежда</a></li>
                            <li><a href="#">Рабочие комбинезоны</a></li>
                            <li><a href="#">Рабочие костюмы</a></li>
                            <li><a href="#">Медицинская одежда</a></li>
                            <li><a href="#">Одежда для обслуживающего персонала</a></li>
                            <li><a href="#">Одежда для охранных и силовых структур</a></li>
                            <li><a href="#">Одежда со специальными защитными свойствами</a></li>
                            <li><a href="#">Сигнальная одежда</a></li>
                            <li><a href="#">Строительная одежда</a></li>
                            <li><a href="#">Камуфлированная одежда</a></li>
                        </ul>
                    </li>
                    <li><a href="">Рабочая обувь</a></li>
                    <li><a href="#">Средства индивидуальной защиты</a></li>
                    <li><a href="#">Рабочие перчатки</a></li>
                    <li><a href="#">Головные уборы</a></li>
                    <li><a href="#">Бытовой текстиль</a></li>
                </ul>

                <?php
                require_once '_left_block.php';
                ?>

            </div>
            <div class="col-xs-9">

                <div class="grid">
                    <div class="element text-right">
                        <input type="checkbox" class="checkbox-switcher" name="" id="btn_c1">
                        <label for="btn_c1">&nbsp;</label>
                    </div>
                    <div class="element text-right">
                        <input type="button" class="button-orange" value="Параметры" data-toggle="modal" data-target="#filterForm">
                    </div>
                    <div class="element text-right">
                        Сортировка
                    </div>
                    <div class="element">
                        <label class="custom-select">
                            <select name="">
                                <option value="">Уменьшение цены</option>
                            </select>
                        </label>
                    </div>
                    <div class="element text-right">
                        Товаров на странице
                    </div>
                    <div class="element">
                        <label class="custom-select">
                            <select name="">
                                <option value="">100</option>
                            </select>
                        </label>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="#" class="product-announce">
                            <img src="../img/example.png" alt=""><br />
                            <img src="../img/icon_clothes_small.png" alt="">
                            &nbsp;
                            <b>Спецодежда</b>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="product-announce">
                            <img src="../img/example.png" alt=""><br />
                            <img src="../img/icon_footwear_small.png" alt="">
                            &nbsp;
                            <b>Рабочая обувь</b>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="product-announce">
                            <img src="../img/example.png" alt=""><br />
                            <img src="../img/icon_protection_small.png" alt="">
                            &nbsp;
                            <b>Средства защиты</b>
                        </a>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="#" class="product-announce">
                            <img src="../img/example.png" alt=""><br />
                            <img src="../img/icon_gloves_small.png" alt="">
                            &nbsp;
                            <b>Рабочие перчатки</b>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="product-announce">
                            <img src="../img/example.png" alt=""><br />
                            <img src="../img/icon_headdress_small.png" alt="">
                            &nbsp;
                            <b>Головные уборы</b>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="product-announce">
                            <img src="../img/example.png" alt=""><br />
                            <img src="../img/icon_textile_small.png" alt="">
                            &nbsp;
                            <b>Бытовой текстиль</b>
                        </a>
                    </div>
                </div>

                <h1 class="text-center"><img src="../img/icon_clothes.png" alt=""> Спецодежда</h1>
                <br>
                <div class="announce">
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                </div>
                <div class="announce">
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                </div>

                <br>
                <h1 class="text-center"><img src="../img/icon_footwear.png" alt=""> Рабочая обувь</h1>
                <br>
                <div class="announce">
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                </div>
                <div class="announce">
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                </div>

                <br>
                <h1 class="text-center"><img src="../img/icon_protection.png" alt=""> Средства индивидуальной защиты
                </h1>
                <br>
                <div class="announce">
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                </div>
                <div class="announce">
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                    <div class="element">
                        <a href="#" class="orange">
                            <img src="../img/example.png" alt=""><br />
                            Зимняя спецодежда
                        </a>
                    </div>
                </div>

                <ul class="pagination">
                    <li class="disabled"><a href="#"><span aria-hidden="true">&laquo;</span></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#"><span aria-hidden="true">»</span></a></li>
                </ul>

            </div>
        </div>
    </div>

<?php
require_once '_footer.php';
?>