<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <a href="#">Поиск</a>
                </div>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">Скачать каталог</a> <img src="../img/icon_pdf.png" alt="" />
                &nbsp;
                &nbsp;
                <a href="#" class="orange">Скачать прайс</a> <img src="../img/icon_price.png" alt="" />
            </div>
        </div>

        <h1>Поиск по сайту</h1>

        <div class="standoff-block">
            <div class="row">
                <div class="col-xs-5">
                    <div class="grid">
                        <div class="element">
                            <img src="../img/lens_icon_orange.png" alt="">
                        </div>
                        <div class="element">
                            <input type="text" class="form-control" placeholder="Пошив на заказ">
                        </div>
                        <div class="element">
                            <input type="submit" name="" class="button-orange" value="Искать">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="standoff-block">
            <div class="row">
                <div class="col-xs-6">
                    <b>Результаты поиска по разделам сайта</b>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="#" class="orange">Развернуть (показать еще 10)</a>
                </div>
            </div>
        </div>

        <div class="standoff-block">
            .... предлагает пошив спецодежды на заказ в Москве и по всей России на основе предоставленного клиентом описания, спецификации, технической документации, образца, фотографии или эскиза. Также возможно внесение изменений в существующие модели согласно пожеланиям заказчика и производственной необходимости...
            <br>
            <a href="#" class="orange">Перейти на страницу «Пошив на заказ»</a>
        </div>

        <div class="standoff-block">
            .... предлагает пошив спецодежды на заказ в Москве и по всей России на основе предоставленного клиентом описания, спецификации, технической документации, образца, фотографии или эскиза. Также возможно внесение изменений в существующие модели согласно пожеланиям заказчика и производственной необходимости...
            <br>
            <a href="#" class="orange">Перейти на страницу «Пошив на заказ»</a>
        </div>

        <br>

        <div class="standoff-block">
            <div class="row">
                <div class="col-xs-6">
                    <b>Результаты поиска по товарам</b>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="#" class="orange">Развернуть (показать еще 10)</a>
                </div>
            </div>
        </div>

        <div class="announce2">
            <div class="element">
                <a href="#" class="product-announce-active">
                    <img src="../img/example.png" alt=""><br />
                    <div class="bg">
                        <span class="button-orange">Купить</span>
                    </div>
                    <div class="text">
                        Костюм «Тюмень» песочный с черным
                        <p>
                            <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                            <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                        </p>
                    </div>
                </a>
            </div>
            <div class="element">
                <a href="#" class="product-announce-active">
                    <img src="../img/example.png" alt=""><br />
                    <div class="bg">
                        <span class="button-orange">Купить</span>
                    </div>
                    <div class="text">
                        Костюм «Тюмень» песочный с черным
                        <p>
                            <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                            <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                        </p>
                    </div>
                </a>
            </div>
            <div class="element">
                <a href="#" class="product-announce-active">
                    <img src="../img/example.png" alt=""><br />
                    <div class="bg">
                        <span class="button-orange">Купить</span>
                    </div>
                    <div class="text">
                        Костюм «Тюмень»
                        <p>
                            <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                            <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                        </p>
                    </div>
                </a>
            </div>
            <div class="element">
                <a href="#" class="product-announce-active">
                    <img src="../img/example.png" alt=""><br />
                    <div class="bg">
                        <span class="button-orange">Купить</span>
                    </div>
                    <div class="text">
                        Костюм «Тюмень» песочный с черным
                        <p>
                            <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                            <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                        </p>
                    </div>
                </a>
            </div>
        </div>

        <div class="announce2">
            <div class="element">
                <a href="#" class="product-announce-active">
                    <img src="../img/example.png" alt=""><br />
                    <div class="bg">
                        <span class="button-orange">Купить</span>
                    </div>
                    <div class="text">
                        Костюм «Тюмень» песочный с черным
                        <p>
                            <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                            <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                        </p>
                    </div>
                </a>
            </div>
            <div class="element">
                <a href="#" class="product-announce-active">
                    <img src="../img/example.png" alt=""><br />
                    <div class="bg">
                        <span class="button-orange">Купить</span>
                    </div>
                    <div class="text">
                        Костюм «Тюмень» песочный с черным
                        <p>
                            <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                            <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                        </p>
                    </div>
                </a>
            </div>
            <div class="element">
                <a href="#" class="product-announce-active">
                    <img src="../img/example.png" alt=""><br />
                    <div class="bg">
                        <span class="button-orange">Купить</span>
                    </div>
                    <div class="text">
                        Костюм «Тюмень»
                        <p>
                            <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                            <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                        </p>
                    </div>
                </a>
            </div>
            <div class="element">
                <a href="#" class="product-announce-active">
                    <img src="../img/example.png" alt=""><br />
                    <div class="bg">
                        <span class="button-orange">Купить</span>
                    </div>
                    <div class="text">
                        Костюм «Тюмень» песочный с черным
                        <p>
                            <b>Опт:</b> <span class="orange">4240 Р</span> (<s>1848 Р</s>) <br>
                            <b>Розница:</b> <span class="orange">5820 Р</span> (<s>1848 Р</s>) <br>
                        </p>
                    </div>
                </a>
            </div>
        </div>

    </div>

<?php
require_once '_footer.php';
?>