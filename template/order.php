<?php
require_once '_header.php';
?>

    <div class="container">
        <div class="breadcrumbs">
            <a href="#">Главная</a>
            <span class="glyphicon glyphicon-chevron-right"></span>
            <a href="#">Корзина</a>
            <span class="glyphicon glyphicon-chevron-right"></span>
            <a href="#">Оформление заказа</a>
        </div>

        <h1>Оформление заказа</h1>
        <div class="row">
            <div class="col-xs-4">
                <h4>
                    <div class="header-title-bold">Тип плательщика</div>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="radio" name="type" checked id="btn_r1"> <label for="btn_r1">Физическое лицо</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="radio" name="type" id="btn_r2"> <label for="btn_r2">Юридическое лицо</label>
                        </div>
                    </div>
                </h4>

                <h4><div class="header-title-bold">Ваше имя</div><input type="text" class="form-control" style="width: 70%;"></h4>
                <h4><div class="header-title-bold">Телефон для связи</div><input type="text" class="form-control" value="+7(" style="width: 50%;"></h4>
                <h4><div class="header-title-bold">E-Mail</div><input type="text" class="form-control" value="" style="width: 70%;"></h4>

                <h4>
                    <div class="header-title-bold">Доставка</div>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="radio" name="delivery" checked id="btn_r3"> <label for="btn_r3">самовывоз</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="radio" name="delivery" id="btn_r4"> <label for="btn_r4">Доставка курьером автотранспортом</label>
                        </div>
                    </div>
                </h4>

                <h4 class="text-muted"><div class="header-title-bold">Адрес доставки</div></h4>
                <select name="" id="" class="form-control" disabled>
                    <option value="">Москва</option>
                </select>

                <br>
                <input type="text" name="" class="form-control" disabled value="ул. Ленина, дом 48/15">

                <h4 class="text-muted"><div class="header-title-bold">Комментарий к заказу</div></h4>
                <textarea name="" id="" cols="30" rows="3" class="form-control noresize" disabled></textarea>

                <br>
                <input type="button" class="button-orange" value="Оформить заказ">
                <br>
                <br>
                <br>
                <br>
            </div>
            <div class="col-xs-4">
                <div class="result-block">
                    <div class="text">
                        <p><b>Общий вес: 55.6 кг</b></p>
                        <p><b>Позиций: 3</b></p>
                        <p><b>Единиц товара: 45</b></p>
                    </div>
                    <div class="splitter"></div>
                    <div class="text">
                        <div class="row">
                            <div class="col-xs-7">
                                <b>Общая стоимость:</b>
                            </div>
                            <div class="col-xs-5">
                                <b>126 740 р</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7">
                                <b>С оптовой скидкой:</b><br>
                                <span class="small">(Сумма заказа > 30 000 р)</span>
                            </div>
                            <div class="col-xs-5">
                                <span class="orange"><b>100 800 р</b></span>
                            </div>
                        </div>
                    </div>
                    <div class="splitter"></div>
                    <div class="text">
                        <p><span class="small">Выбрана лоставка курьеров - стоимость доставки будет согласована дополнительно</span></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <b>Товары в заказе</b>
            </div>
            <div class="col-xs-6 text-right">
                <a href="#" class="orange">Вернуться к корзине</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-3"></div>
            <div class="col-xs-2"><b>Стоимость</b></div>
            <div class="col-xs-2"><b>Размер</b></div>
            <div class="col-xs-1"><b>Количество</b></div>
            <div class="col-xs-2 text-center"><b>Сумма</b></div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <b>Костюм "Тюмень" песочный с черным</b>
                    <p>Артикул 40264</p>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <b>Костюм "Тюмень" песочный с черным</b>
                    <p>Артикул 40264</p>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
        </div>

        <div class="cart-block">
            <div class="block1">
                <div>
                    <img src="../img/example.png" alt="" class="img-responsive" style="max-width: 100px;">
                </div>
                <div>
                    <b>Костюм "Тюмень" песочный с черным</b>
                    <p>Артикул 40264</p>
                </div>
                <div class="contents">
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">4 240 р</div>
                        <div class="col-xs-5">88-92/170-176</div>
                        <div class="col-xs-3"><input type="text" class="form-control text-center" value="5" /></div>
                    </div>
                </div>
                <div class="price">
                    73 023 р
                </div>
            </div>
        </div>

    </div>

<?php
require_once '_footer.php';
?>