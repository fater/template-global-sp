$(document).ready(function () {
    /**
     * Нажатие на кнопку + (увеличить количество заказываемого товара)
     */
    $('[data-object="amount_plus"]').click(function () {
        var amount = parseInt($(this).parent().parent().find('input[type="text"]').val()) + 1;
        $(this).parent().parent().find('input[type="text"]').val(amount);
        $('[data-object="addCardNotifier"]').addClass('hidden');
    });

    /**
     * Нажатие на кнопку - (уменьшить количество заказываемого товара)
     */
    $('[data-object="amount_minus"]').click(function () {
        var amount = parseInt($(this).parent().parent().find('input[type="text"]').val()) - 1;
        if (amount < 0) {
            amount = 0;
        }
        $(this).parent().parent().find('input[type="text"]').val(amount);
        $('[data-object="addCardNotifier"]').addClass('hidden');
    });

    /**
     * Нажатие на кнопку добавления в корзину
     */
    $('[data-object="buyButton"]').click(function () {
        var notNullFlag = false;
        $('[data-object="amount_box"] input[type="text"]').each(function () {
            if (parseInt($(this).val()) > 0) {
                notNullFlag = true;
                return false;
            }
        });
        if (notNullFlag) {
            alert('Submit')
        } else {
            $('[data-object="addCardNotifier"]').removeClass('hidden');
        }
    });

    /**
     * Slider
     */
    if ($('#slider1 .active').length) {
        setInterval('slider1()', 7000);
    }

    $('div[data-object="roll"]').click(function () {
        if ($(this).find('span').hasClass('glyphicon-chevron-down')) {
            $('div[data-id="' + $(this).attr('data-name') + '"]').removeClass('hidden');
            $(this).find('span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        } else {
            $('div[data-id="' + $(this).attr('data-name') + '"]').addClass('hidden');
            $(this).find('span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        }
    });

    /**
     * Seach form show
     */
    $('#searchFormOpen').click(function () {
        $('#searchFormBlock').removeClass('hidden');
    });

    /**
     * Seach form hide
     */
    $('#searchFormClose').click(function () {
        $('#searchFormBlock').addClass('hidden');
    });

    /**
     * Index Announce form close
     */
    $('#indexFormAnnounceClose').click(function () {
        $('#indexFormAnnounce').addClass('hidden');
    });

    /**
     * Carousel
     */
    $('#slider').bxSlider({
        slideWidth: 260,
        minSlides: 4,
        maxSlides: 4,
        slideMargin: 30,
        moveSlides: 1,
        pager: false
    });
    var divs = $("#slider .slide");
    var max = 0;
    for (var i = 0; i < divs.length; i++) {
        max = Math.max(max, $(divs[i]).height());
    }
    $(divs).css('min-height', max + 'px');
    $("#slider .slide a").css('height', max + 'px');
});


function slider1() {
    var $active = $('#slider1 .active');
    var $next = ($active.next().length > 0) ? $active.next() : $('#slider img:first');
    $next.css('z-index', 2);//move the next image up the pile
    $active.fadeOut(1500, function () {//fade out the top image
        $active.css('z-index', 1).show().removeClass('active');//reset the z-index and unhide the image
        $next.css('z-index', 3).addClass('active');//make the next image the top one
    });
}